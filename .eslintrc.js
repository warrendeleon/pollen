const error = 2
const warning = 1

module.exports = {
  plugins: ['react', 'react-native', 'jest', 'prettier'],
  extends: ['plugin:prettier/recommended'],
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
      jsx: true
    },
    sourceType: 'module'
  },
  rules: {
    'react/jsx-uses-react': error,
    'react/jsx-uses-vars': error,
    'react/jsx-filename-extension': [
      warning,
      {
        extensions: ['.js']
      }
    ],
    'linebreak-style': [error, 'unix'],
    quotes: [warning, 'single', { avoidEscape: true }],
    semi: [error, 'never'],
    'prefer-arrow-callback': error,
    'react/jsx-no-bind': [
      error,
      {
        ignoreRefs: true
      }
    ],
    'react-native/no-inline-styles': error,
    'react-native/no-unused-styles': error,
    'no-unused-vars': error,
    'no-undef': error,
    'no-console': error,
    "sort-imports": "error",
    "sort-vars": "error",
    "sort-keys": ["error", "asc", { caseSensitive: true, natural: true }],
  },
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    node: true,
    jest: true
  },
  globals: {
    __DEV__: true,
    context: true,
    detox: false,
    device: false,
    expect: false,
    waitFor: false,
    element: false,
    by: false,
    GLOBAL: false
  }
}
