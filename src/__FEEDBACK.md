 - Personally I would have the backend in a different project.
 - On the home screen I changed the button location to the bottom, so it's easy to reach with one hand.
 - I installed react-native-elements to get predefined components like the card component, and a pre-styled button.
 - I added a flatlist instead of simply rendering components on the scrollView. To have the ability to auto-scroll.
 - I restructured the code and separated into smaller components in different files to improve readability.
 - On the form, I installed a library to move the form up when keyboard is present.
 - I added formik as a form library and yup and the validation library.
 - Submit of the form is disabled if the form is not fill in.
 - Relevant error message is displayed after focusing on another form field.
 - I installed and developed integration tests with Detox. A library specifically made for React Native. To run you need to install applesimutils `brew tap
  wix/brew && brew install applesimutils` and run `yarn run e2e:release` with the node server online. The tests will test happy and sad paths. Like form
   validation errors.
- There's a folder called demo with screenshots of the end result. Also there a demo video of the app working and a video of the detox integration tests.
 
 
 
 
