/* eslint-disable react/jsx-no-bind */
import 'react-native-gesture-handler'
import { Confirm } from './screens/Confirm'
import { Event } from './screens/Event'
import { Home } from './screens/Home'
import React from 'react'
import { Schedule } from './screens/Schedule'
import { createStackNavigator } from '@react-navigation/stack'

export const Navigator = () => {
  const Stack = createStackNavigator()

  return (
    <Stack.Navigator
      screenOptions={{
        headerBackTitleVisible: false,
        headerStyle: {
          backgroundColor: '#F2F2F2'
        },
        headerTintColor: '#404040',
        headerTitleStyle: {
          fontWeight: 'bold'
        }
      }}
    >
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Schedule" component={Schedule} />
      <Stack.Screen
        name="Event"
        component={Event}
        options={({ route }) => ({ title: route.params.title })}
      />
      <Stack.Screen
        name="Confirm"
        component={Confirm}
        options={({ route }) => ({ title: route.params.title })}
      />
    </Stack.Navigator>
  )
}
