import SoundPlayer from 'react-native-sound-player'

export const playSound = () => {
  // Choose a random sound to play
  const SOUNDS = ['crowd', 'woohoo', 'clapping'] // each has an mp3 file in /ios
  const sound = SOUNDS[Math.ceil(Math.random() * SOUNDS.length) - 1]

  // Play the sound via SoundManager
  try {
    SoundPlayer.playSoundFile(sound, 'mp3')
  } catch (e) {
    console.log('sound not implemented')
  }
}
