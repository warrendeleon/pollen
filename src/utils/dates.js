import moment from 'moment'

export const getClosestDate = (date, events) => {
  const filteredEvents = events
    .filter((event) => moment(event.title, 'DD-MM-YYYY').isSameOrAfter(date))
    .reverse()

  return events.indexOf(filteredEvents[0])
}
