export const groupEventsByDay = (events) => {
  const sections = []

  events.forEach((event) => {
    const thisDay = new Date(event.starts).toLocaleDateString('en-GB')

    const item = sections.find((item) => item.title === thisDay)

    if (item) {
      item.data.push(event)
    } else {
      sections.push({
        data: [event],
        title: thisDay
      })
    }
  })
  return sections.sort().reverse()
}
