import { ActivityIndicator, StyleSheet, Text, View } from 'react-native'
import { Button } from 'react-native-elements'
import { GET_EVENTS } from '../../graphql/queries'
import React, { useCallback } from 'react'
import { responsiveSize } from '../../utils/responsive-size'
import { useQuery } from '@apollo/react-hooks'

export const Home = ({ navigation: { navigate } }) => {
  const { loading, error, data } = useQuery(GET_EVENTS)

  if (loading) {
    return (
      <View>
        <ActivityIndicator />
      </View>
    )
  }
  if (error) {
    return (
      <View>
        <Text>ERROR: Is the server running?</Text>
      </View>
    )
  }

  return (
    <View testID={'home.screen'} style={styles.container}>
      <View style={styles.mainContent}>
        <Text style={styles.header}>EventsApp</Text>
        <Text style={styles.blurb}>
          The worlds greatest events in one tiny app.
        </Text>
      </View>
      <View style={styles.buttonContainer}>
        <Button
          testID={'home.btn.allevents'}
          onPress={() => navigate('Schedule', { data: data })}
          buttonStyle={styles.button}
          title="All Events"
          color="#841584"
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  blurb: {
    fontSize: responsiveSize(18),
    marginBottom: responsiveSize(12),
    textAlign: 'center'
  },
  button: {
    backgroundColor: '#404040',
    height: responsiveSize(47)
  },
  buttonContainer: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
    justifyContent: 'space-between',
    padding: responsiveSize(16)
  },
  header: {
    fontSize: responsiveSize(36),
    marginVertical: responsiveSize(24),
    textAlign: 'center'
  },
  mainContent: {
    flex: 4,
    justifyContent: 'center'
  }
})
