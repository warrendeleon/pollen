import { Dimensions, FlatList, StyleSheet, Text, View } from 'react-native'
import React, { useCallback } from 'react'
import { Icon } from 'react-native-elements'
import { responsiveSize } from '../../utils/responsive-size'

const { width: SCREEN_WIDTH } = Dimensions.get('window')

export const DateFlatList = ({
  dateFlatList,
  groupedEvents,
  onViewableItemsChanged,
  testID
}) => {
  const getItemLayout = useCallback(
    (data, index) => ({
      index,
      length: SCREEN_WIDTH,
      offset: SCREEN_WIDTH * index
    }),
    []
  )
  const keyExtractor = useCallback((item, index) => item.title + index, [])
  const renderItem = useCallback(({ item: { title }, index }) => {
    return (
      <View style={styles.dateContainer}>
        {index === 0 && (
          <View style={styles.iconContainer}>
            <Icon
              name={'gesture-swipe-horizontal'}
              type={'material-community'}
              color={'black'}
              size={25}
            />
          </View>
        )}
        <Text style={styles.textContainer}>{title}</Text>
      </View>
    )
  }, [])

  return (
    <View style={styles.container}>
      <FlatList
        testID={testID}
        ref={dateFlatList}
        horizontal={true}
        showsHorizontalScrollIndicator={true}
        snapToAlignment={'center'}
        data={groupedEvents}
        onViewableItemsChanged={onViewableItemsChanged.current}
        getItemLayout={getItemLayout}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
        viewabilityConfig={{
          itemVisiblePercentThreshold: 50
        }}
        snapToInterval={SCREEN_WIDTH}
        decelerationRate={'fast'}
        pagingEnabled
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    height: responsiveSize(50),
    width: '100%'
  },
  dateContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    height: responsiveSize(50),
    justifyContent: 'center',
    width: SCREEN_WIDTH
  },
  iconContainer: {
    left: responsiveSize(16),
    position: 'absolute'
  },
  textContainer: {
    marginHorizontal: responsiveSize(10),
    textAlign: 'center'
  }
})
