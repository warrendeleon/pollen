import { Button as RNButton, StyleSheet, View } from 'react-native'
import React, { useCallback } from 'react'
import DateTimePicker from '@react-native-community/datetimepicker'
import { responsiveSize } from '../../utils/responsive-size'

export const DatePicker = ({
  date,
  firstEventDate,
  lastEventDate,
  onChange,
  setModalVisibility,
  showModal
}) => {
  const doneOnPress = useCallback(() => setModalVisibility(false), [])
  const resetOnPress = useCallback(() => onChange(null, lastEventDate), [])

  return (
    <View style={styles.modalContainer}>
      <View style={styles.modalButton}>
        <RNButton
          title={'Reset'}
          accessibilityLabel="Clone date picker"
          onPress={resetOnPress}
        />
        <RNButton
          title={'Done'}
          accessibilityLabel="Clone date picker"
          onPress={doneOnPress}
        />
      </View>
      <DateTimePicker
        show={showModal}
        testID="dateTimePicker"
        value={date}
        onChange={onChange}
        maximumDate={lastEventDate}
        minimumDate={firstEventDate}
        mode={'date'}
        is24Hour={true}
        display="default"
      />
    </View>
  )
}

const styles = StyleSheet.create({
  modalButton: {
    alignItems: 'flex-end',
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: responsiveSize(8),
    width: '100%'
  },
  modalContainer: { backgroundColor: '#F2F2F2' }
})
