import { Button, Card } from 'react-native-elements'
import React, { useCallback } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import moment from 'moment'
import { responsiveSize } from '../../utils/responsive-size'
import { useNavigation } from '@react-navigation/native'

export const EventCard = ({ id, starts, title, description, image }) => {
  const navigation = useNavigation()
  const onPress = useCallback(
    () =>
      navigation.navigate('Event', {
        description,
        id,
        image,
        starts,
        title
      }),
    []
  )

  return (
    <Card
      title={title}
      image={{ uri: image }}
      titleStyle={styles.cardHeading}
      style={styles.card}
    >
      <View>
        <Text testID={'schedule.event.date'} style={styles.date}>
          {moment(starts).format('dddd, Do MMMM YYYY, H:mm')}
        </Text>
        <Text testID={'schedule.event.description'} style={styles.description}>
          {description}
        </Text>
      </View>

      <Button
        testID={'schedule.event.btn'}
        buttonStyle={styles.button}
        onPress={onPress}
        title="Buy a ticket"
        accessibilityLabel="Buy a ticket for this event"
      />
    </Card>
  )
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#404040',
    height: responsiveSize(47)
  },
  card: {
    backgroundColor: '#FFFFFF',
    marginHorizontal: responsiveSize(10),
    marginVertical: responsiveSize(8)
  },
  cardHeading: {
    fontSize: responsiveSize(18),
    fontWeight: 'bold',
    textAlign: 'center'
  },
  date: {
    fontSize: responsiveSize(12),
    fontWeight: 'bold',
    marginBottom: responsiveSize(5)
  },
  description: {
    fontSize: responsiveSize(12),
    marginBottom: responsiveSize(15),
    marginTop: responsiveSize(10),
    textAlign: 'justify'
  }
})
