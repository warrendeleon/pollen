import { FlatList, StyleSheet, View } from 'react-native'
import React, { useCallback, useLayoutEffect, useRef, useState } from 'react'
import { Button } from 'react-native-elements'
import { DateFlatList } from './date-flat-list'
import { DatePicker } from './date-picker'
import { EventCard } from './event-card'
import { getClosestDate } from '../../utils/dates'
import { groupEventsByDay } from '../../utils/events'
import moment from 'moment'
import { responsiveSize } from '../../utils/responsive-size'

const renderItem = ({ item, key }) => <EventCard key={key} {...item} />

export const Schedule = ({ route, navigation }) => {
  const dateFlatList = useRef()
  const [showModal, setModalVisibility] = useState(false)
  const groupedEvents = groupEventsByDay(route.params.data.allEvents)
  const lastEventDate = moment(groupedEvents[0].title, 'DD-MM-YYYY').toDate()
  const firstEventDate = moment(
    groupedEvents[groupedEvents.length - 1].title,
    'DD-MM-YYYY'
  ).toDate()
  const [date, setDate] = useState(lastEventDate)
  const [events, setEvents] = useState(groupedEvents[0].data)

  const onViewableItemsChanged = useRef(({ viewableItems }) => {
    setDate(moment(viewableItems[0].item.title, 'DD-MM-YYYY').toDate())
    setEvents(viewableItems[0].item.data)
  })

  const onChange = useCallback((event, selectedDate) => {
    setDate(selectedDate)
    const indexEvent = getClosestDate(selectedDate, groupedEvents)
    setEvents(groupedEvents[indexEvent].data)
    dateFlatList.current.scrollToIndex({ index: indexEvent })
  }, [])
  const calendarOnPress = useCallback(() => setModalVisibility(true), [])

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Button
          testID={'schedule.datepicker.btn'}
          onPress={calendarOnPress}
          buttonStyle={styles.calendar}
          icon={{
            color: '#404040',
            name: 'calendar',
            type: 'entypo'
          }}
        />
      )
    })
  }, [navigation])

  return (
    <View testID={'schedule.screen'} style={styles.container}>
      <DateFlatList
        testID={'schedule.date.flatlist'}
        dateFlatList={dateFlatList}
        groupedEvents={groupedEvents}
        onViewableItemsChanged={onViewableItemsChanged}
      />
      <FlatList data={events} renderItem={renderItem} />

      {showModal && (
        <DatePicker
          date={date}
          firstEventDate={firstEventDate}
          lastEventDate={lastEventDate}
          onChange={onChange}
          setModalVisibility={setModalVisibility}
          showModal={showModal}
        />
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  calendar: {
    backgroundColor: 'transparent',
    paddingRight: responsiveSize(8)
  },
  container: {
    flex: 1
  }
})
