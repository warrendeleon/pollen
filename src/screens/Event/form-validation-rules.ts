import { object as yupObject, string as yupString } from 'yup'

export const bookEventValidationRules = yupObject().shape({
  email: yupString()
    .email(() => 'Not a valid email format.')
    .required(() => 'Email is a required field.'),
  name: yupString().required(() => 'Name is required field.')
})
