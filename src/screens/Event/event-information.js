import { StyleSheet, Text, View } from 'react-native'
import { Image } from 'react-native-elements'
import PropTypes from 'prop-types'
import React from 'react'
import moment from 'moment'
import { responsiveSize } from '../../utils/responsive-size'

export const EventInformation = ({ event }) => {
  const { starts, image, description } = event

  return (
    <View testID={'event.info'} style={styles.container}>
      <View style={styles.cardImage}>
        <Image
          source={{ uri: image }}
          style={styles.cardImageCanvas}
          resizeMode="cover"
        />
      </View>
      <View style={styles.section}>
        <Text style={styles.date}>
          {moment(starts).format('dddd, DD/MM/YYYY, H:mm')}
        </Text>
        <Text style={styles.description}>{description}</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  cardImage: {
    flex: 0
  },
  cardImageCanvas: {
    height: responsiveSize(115),
    width: '100%'
  },
  container: {
    flex: 1,
    width: '100%'
  },
  date: {
    fontSize: responsiveSize(12),
    fontWeight: 'bold'
  },
  description: {
    flex: 1,
    fontSize: responsiveSize(12),
    marginTop: responsiveSize(10),
    textAlign: 'justify'
  },
  section: {
    flex: 1,
    padding: responsiveSize(16)
  }
})

EventInformation.propTypes = {
  event: PropTypes.shape({
    image: PropTypes.string.isRequired,
    location: PropTypes.shape({
      latitude: PropTypes.string,
      longitude: PropTypes.string
    }),
    price: PropTypes.decimal,
    starts: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
  })
}
