import { Button, Input } from 'react-native-elements'
import React, { useCallback, useLayoutEffect, useRef } from 'react'
import { Linking, StyleSheet, Text, View } from 'react-native'
import { EventInformation } from './event-information'
import { Formik } from 'formik'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import PropTypes from 'prop-types'
import { bookEventValidationRules } from './form-validation-rules'
import { responsiveSize } from '../../utils/responsive-size'

const DEMO_EVENT = {
  location: {
    latitude: '51.5079891',
    longitude: '-0.1278803'
  },
  price: '$50.39'
}

export const Event = ({
  navigation,
  navigation: { reset },
  route: {
    params: { id, title, starts },
    params
  }
}) => {
  const emailInputText = useRef()
  const moveFocusToEmail = useCallback(() => emailInputText.current.focus(), [])
  const onSubmit = useCallback(
    ({ name, email }) => {
      reset({
        index: 0,
        routes: [
          { name: 'Confirm', params: { date: starts, email, id, name, title } }
        ]
      })
    },
    [reset]
  )

  useLayoutEffect(() => {
    const { latitude, longitude } = DEMO_EVENT

    navigation.setOptions({
      headerRight: () => (
        <Button
          onPress={() =>
            Linking.openURL(
              `https://www.google.co.uk/maps/?q=${latitude},${longitude}`
            )
          }
          buttonStyle={styles.map}
          icon={{
            color: '#404040',
            name: 'map',
            type: 'entypo'
          }}
        />
      )
    })
  }, [navigation])

  return (
    <View testID={'event.screen'} style={styles.container}>
      <Formik
        initialValues={{
          email: '',
          name: ''
        }}
        onSubmit={onSubmit}
        validationSchema={bookEventValidationRules}
      >
        {(formikProps) => {
          const showErrorMessage = (type) => {
            return formikProps.touched[type] && formikProps.errors[type]
              ? formikProps.errors[type]
              : undefined
          }

          return (
            <>
              <KeyboardAwareScrollView
                testID={'scrollView'}
                extraScrollHeight={50}
              >
                <View>
                  <EventInformation
                    event={{
                      ...params,
                      location: DEMO_EVENT.location
                    }}
                  />
                  <View style={styles.section}>
                    <Text style={styles.header}>Buy a ticket</Text>
                    <Input
                      testID={'event.textinput.name'}
                      accessibilityLabel={'Name'}
                      autoCompleteType={'name'}
                      autoCapitalize={'words'}
                      containerStyle={styles.inputContainer}
                      errorMessage={showErrorMessage('name')}
                      returnKeyType={'next'}
                      label={'Your name'}
                      labelStyle={styles.label}
                      onBlur={formikProps.handleBlur('name')}
                      onChangeText={formikProps.handleChange('name')}
                      onSubmitEditing={moveFocusToEmail}
                      value={formikProps.values.name}
                    />
                    <Input
                      testID={'event.textinput.email'}
                      ref={emailInputText}
                      accessibilityLabel={'Email'}
                      autoCompleteType={'email'}
                      autoCapitalize={'none'}
                      containerStyle={[styles.inputContainer, styles.marginTop]}
                      errorMessage={showErrorMessage('email')}
                      keyboardType={'email-address'}
                      label={'Email address'}
                      labelStyle={styles.label}
                      multiline={false}
                      onBlur={formikProps.handleBlur('email')}
                      onChangeText={formikProps.handleChange('email')}
                      onSubmitEditing={formikProps.handleSubmit}
                      returnKeyType={'go'}
                      textContentType="emailAddress"
                      value={formikProps.values.email}
                    />
                    <View style={styles.emptyView} />
                  </View>
                </View>
              </KeyboardAwareScrollView>
              <View style={styles.buttonContainer}>
                <Button
                  testID={
                    !formikProps.dirty || !formikProps.isValid
                      ? 'event.form.submit.btn.disabled'
                      : 'event.form.submit.btn.enabled'
                  }
                  disabled={!formikProps.dirty || !formikProps.isValid}
                  buttonStyle={styles.button}
                  onPress={() => formikProps.handleSubmit()}
                  title={`Pay ${DEMO_EVENT.price}`}
                />
              </View>
            </>
          )
        }}
      </Formik>
    </View>
  )
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#404040',
    height: responsiveSize(47)
  },
  buttonContainer: {
    backgroundColor: 'white',
    bottom: 0,
    flex: 0,
    padding: responsiveSize(16),
    position: 'absolute',
    width: '100%'
  },
  container: {
    backgroundColor: 'white',
    flex: 1
  },
  emptyView: { height: responsiveSize(100) },
  header: {
    fontSize: responsiveSize(18),
    fontWeight: 'bold',
    marginBottom: responsiveSize(10),
    marginTop: responsiveSize(5),
    textAlign: 'center'
  },
  inputContainer: {
    paddingHorizontal: 0
  },
  label: {
    fontSize: responsiveSize(12)
  },
  map: {
    backgroundColor: 'transparent',
    paddingRight: responsiveSize(8)
  },
  marginTop: {
    marginTop: responsiveSize(10)
  },
  section: {
    paddingHorizontal: responsiveSize(16)
  }
})

Event.propTypes = {
  navigation: PropTypes.shape({})
}
