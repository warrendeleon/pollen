import { Image, StyleSheet, Text, View } from 'react-native'
import React, { useCallback, useEffect } from 'react'
import { Button } from 'react-native-elements'
import moment from 'moment'
import { playSound } from '../../utils/sound'
import { responsiveSize } from '../../utils/responsive-size'

export const Confirm = ({
  navigation: { reset },
  route: {
    params: { date, email, name, title }
  }
}) => {
  useEffect(() => {
    // Play sound effect on confirmation of payment
    setTimeout(() => {
      playSound()
    }, 1000)
  }, [])
  const onPress = useCallback(
    () =>
      reset({
        index: 0,
        routes: [{ name: 'Home' }]
      }),
    [reset]
  )

  return (
    <View style={styles.container}>
      <Image
        source={{
          uri: 'https://media.giphy.com/media/U15x0bURfSEOJI0nbX/giphy.gif'
        }}
        style={styles.image}
      />
      <View style={styles.content}>
        <Text style={styles.header}>Your Ticket</Text>

        <Text style={styles.body}>Event name: {title}</Text>
        <Text style={styles.body}>
          Event date: {moment(date).format('dddd, Do MMM YYYY, H:mm')}
        </Text>
        <Text style={styles.body}>Name: {name}</Text>
        <Text style={styles.body}>Email: {email}</Text>
      </View>
      <View style={styles.buttonContainer}>
        <Button
          onPress={onPress}
          buttonStyle={styles.button}
          title="Go back Home"
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  body: {
    fontSize: responsiveSize(12)
  },
  button: {
    backgroundColor: '#404040',
    height: responsiveSize(47)
  },
  buttonContainer: {
    backgroundColor: 'white',
    bottom: 0,
    flex: 0,
    padding: responsiveSize(16),
    position: 'absolute',
    width: '100%'
  },
  container: {
    backgroundColor: 'white',
    flex: 1
  },
  content: {
    alignItems: 'flex-start',
    flex: 1,
    padding: responsiveSize(16)
  },
  header: {
    alignSelf: 'center',
    fontSize: responsiveSize(24),
    marginBottom: responsiveSize(24)
  },
  image: {
    height: responsiveSize(220),
    resizeMode: 'cover'
  }
})
