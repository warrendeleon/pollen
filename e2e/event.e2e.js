describe('Event Screen', () => {
  beforeEach(async () => {
    await device.reloadReactNative()
    await expect(element(by.id('home.screen'))).toBeVisible()
    await expect(element(by.id('home.btn.allevents'))).toBeVisible()
    await element(by.id('home.btn.allevents')).tap()
    await element(by.text('Buy a ticket')).atIndex(0).tap()
  })

  it('should render.', async () => {
    await expect(element(by.id('event.screen'))).toBeVisible()
  })

  it('should render text field name.', async () => {
    await expect(element(by.id('event.textinput.name'))).toBeVisible()
  })

  it('should render text field email.', async () => {
    await expect(element(by.id('event.textinput.email'))).toBeVisible()
  })

  it('should render disabled form submit btn.', async () => {
    await expect(element(by.id('event.form.submit.btn.disabled'))).toBeVisible()
  })

  it('should render enabled form submit btn.', async () => {
    await element(by.id('event.textinput.name')).tap()
    await element(by.id('event.textinput.name')).typeText('Warren de Leon')
    await element(by.id('event.textinput.email')).tap()
    await element(by.id('event.textinput.email')).typeText(
      'test@warrendeleon.com'
    )
    await element(by.id('scrollView')).tap({ x: 1, y: 10 })
    await expect(element(by.id('event.form.submit.btn.enabled'))).toBeVisible()
  })

  it("should render error when text field 'name' is empty.", async () => {
    await element(by.id('event.textinput.name')).tap()
    await element(by.id('scrollView')).tap({ x: 1, y: 10 })
    await expect(element(by.text('Name is required field.'))).toBeVisible()
  })

  it("should render error when text field 'email' is empty.", async () => {
    await element(by.id('event.textinput.email')).tap()
    await element(by.id('event.textinput.name')).tap()
    await element(by.id('scrollView')).scrollTo('bottom')
    await expect(element(by.text('Email is a required field.'))).toBeVisible()
  })

  it("should render error when text field 'email' is not a valid email.", async () => {
    await element(by.id('event.textinput.email')).tap()
    await element(by.id('event.textinput.email')).typeText('Not a valid email')
    await element(by.id('event.textinput.name')).tap()
    await element(by.id('scrollView')).scrollTo('bottom')
    await expect(element(by.text('Not a valid email format.'))).toBeVisible()
  })
})
