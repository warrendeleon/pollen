describe('Home Screen', () => {
  beforeEach(async () => {
    await device.reloadReactNative()
  })

  it('should render.', async () => {
    await expect(element(by.id('home.screen'))).toBeVisible()
  })

  it('should display text EventsApp', async () => {
    await expect(element(by.text('EventsApp'))).toBeVisible()
  })

  it('should display app description', async () => {
    await expect(
      element(by.text('The worlds greatest events in one tiny app.'))
    ).toBeVisible()
  })
  it("should display btn 'all events'", async () => {
    await expect(element(by.id('home.btn.allevents'))).toBeVisible()
  })
})
