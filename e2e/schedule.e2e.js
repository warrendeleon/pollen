describe('Schedule Screen', () => {
  beforeEach(async () => {
    await device.reloadReactNative()
    await expect(element(by.id('home.screen'))).toBeVisible()
    await expect(element(by.id('home.btn.allevents'))).toBeVisible()
    await element(by.id('home.btn.allevents')).tap()
  })

  it('should render.', async () => {
    await expect(element(by.id('schedule.screen'))).toBeVisible()
  })

  it("should render 'Buy a ticket' btn.", async () => {
    await expect(element(by.text('Buy a ticket')).atIndex(0)).toBeVisible()
  })
  describe('DatePicker', () => {
    it('should render header right calendar btn.', async () => {
      await expect(
        element(by.id('schedule.datepicker.btn')).atIndex(0)
      ).toBeVisible()
    })

    it('should show the datepicker on tap of header right.', async () => {
      await element(by.id('schedule.datepicker.btn')).atIndex(0).tap()
      await expect(element(by.id('dateTimePicker'))).toExist()
    })

    it('should change the flatlist date.', async () => {
      await element(by.id('schedule.datepicker.btn')).atIndex(0).tap()
      await element(by.type('UIPickerView')).setColumnToValue(0, 'July')
      await element(by.type('UIPickerView')).setColumnToValue(1, '20')
      await element(by.text('Done')).atIndex(0).tap()
      await expect(element(by.text('20/07/2020')).atIndex(0)).toBeVisible()
    })
  })

  describe('Date flatlist', () => {
    it('should render the date flatlist.', async () => {
      await expect(element(by.id('schedule.date.flatlist'))).toBeVisible()
    })

    it('should swipe the date flatlist.', async () => {
      await element(by.id('schedule.date.flatlist')).swipe('left')
    })
  })
})
